console.log("Hello world!");

// [SECTION] JSON Object
/*
	-JSON stands for JavaScript Object Notation
	-JSON is also used in other programming languages
	-Core JavaScript has a built-in JSON ojects that contains methods for passing JSON objects
	-JSON is used for serializing different data types into bytes
*/

// JSON Object
/*
	JSON also uses the key/value pairs just like the object properties in JavaScript
	-	Key/Property names should be enclosed with double qoutes
	SYNTAX:
	{
		"propertyA" : "valueA",
		"propertyB" : "valueB"
	}

*/

/*
{
	"city" : "Quezon City",
	"province" : "Metro Manila",
	"country" : "Philippines"
}
*/

// JSON Arrays
// Arrays in JSON are almost same as arrays in JavaScript
// Arrays in JSON Object
/*
	"cities" : [
	{		
		"city" : "Quezon City",
		"province" : "Metro Manila",
		"country" : "Philippines"
	},
	{
		"city" : "Manila City",
		"province" : "Metro Manila",
		"country" : "Philippines"
	},
	{
		"city" : "Makati City",
		"province" : "Metro Manila",
		"country" : "Philippines"
	}
]
*/

// [SECTION] JSON methods
// The "JSON Object" contains methods for parsing and converting data in stringfied JSON
// JSON data is sent or received in text-only(String) format

// Converting Data Intro Stringified JSON

let batchesArr = [
{
	batName: 230,
	schedule: "Part Time"
},
{
	batName: 240,
	schedule: "Full Time"
}

	];
console.log(batchesArr);
console.log("Result from stringfy methods: ");
// Syntax : JSON.stringify(arrays/objects);
console.log(JSON.stringify(batchesArr));

let data = JSON.stringify(
{
	name: "John",
	age: 31,
	address: {
		"city" : "Manila City",
		"province" : "Metro Manila",
		"country" : "Philippines"
	}
}
	);
console.log(data);

/*// User Details
let firstName = prompt("Enter your first name: ");
let lastName = prompt("Enter your last name: ");
let email = prompt("Enter your email: ");
let password = prompt("Enter your password: ")

let userDetails = JSON.stringify(
{
	firstName : firstName,
	lastName : lastName,
	email : email,
	password : pssword
	}
);
console.log(userDetails);*/

// [SECTION] Coverting stringified JSON into JavaScript Objects:

let batchesJSON = `[
{
	"batName": 230,
	"schedule": "Part Time"
},
{
	"batName": 240,
	"schedule": "Full Time"
}

]`;
console.log("batchesJSON content: ");
console.log(batchesJSON);

console.log("Result from parse method: ");
let parseBatches = JSON.parse(batchesJSON);
console.log(parseBatches);
console.log(parseBatches[0].batchName);

let stringifiedObject = `
	{
		"name" : "John",
		"age" : 31,
		"address" :{
			"city" : "Manila",
			"country" : "Philippines"
		}
	}	
`
console.log(stringifiedObject);
console.log(JSON.parse(stringifiedObject));

